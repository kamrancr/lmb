provider "aws" {
  region = "eu-west-1"
}

locals {
    name = "Kamran-LMB-Dev"
    owner = "Kamran Ali"
    key_name = "kamran-crlab"
}

data "aws_ami" "lmb" {
    most_recent = true
    filter {
        name = "name"
        values = ["kamran-lmb*"]
    }

    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }

    owners = ["373819956540"]
}

module "network" {
    source = "../../modules/network"
    vpc_cidr = "10.24.0.0/16"
    vpc_dns = true

    public_subnets = ["10.24.0.0/24", "10.24.1.0/24"]

    private_subnets = ["10.24.255.0/24", "10.24.254.0/24"]

    name = "${local.name}"
    owner = "${local.owner}"
    count = 2
    az = ["eu-west-1a", "eu-west-1b"]

    create_bastion = true
}

module "compute" {
    source = "../../modules/compute"
    web_ami = "${data.aws_ami.lmb.id}"
    web_instance_type = "t2.micro"
    web_key_name = "${local.key_name}"
    web_security_group_ids = ["${module.network.private_sg_id}"]
    web_min = 2
    web_max = 4

    app_ami = "${data.aws_ami.lmb.id}"
    app_instance_type = "t2.micro"
    app_key_name = "${local.key_name}"
    app_security_group_ids = ["${module.network.private_sg_id}"]
    app_min = 2
    app_max = 4

    private_subnets = ["${module.network.private_subnet_ids}"]
    
    public_subnet_ids = ["${module.network.public_subnet_ids}"]
    web_security_groups = ["${module.network.public_sg_id}"]

    private_subnet_ids = ["${module.network.private_subnet_ids}"]
    app_security_groups = ["${module.network.private_sg_id}"]

    dns_zone = "kamran.colab.cloudreach.com"
    env = "dev"

    create_bastion = true
    bastion_instance_type = "t2.micro"
    bastion_key_name = "${local.key_name}"
    bastion_sg_ids = ["${module.network.bastion_sg_id}"]

    bastion_min = 1
    bastion_max = 2
    public_subnets = ["${module.network.public_subnet_ids}"]

    name = "${local.name}"
    owner = "${local.owner}"
}

module "database" {
    source = "../../modules/database"
    subnet_ids = ["${module.network.private_subnet_ids}"]
    db_instance = "db.t2.micro"
    db_name = "littlemamasbakerydev"
    db_user = "littlemamasbakery"
    db_password = "sup3rsecur3p45s4seb"
    skip_snapshot = true
    sg_id = "${module.network.private_sg_id}"

    engine = "memcached"
    node_type = "cache.t2.micro"
    port = 11211
    pg_name = "default.memcached1.4"

    name = "${local.name}"
    owner = "${local.owner}"
}