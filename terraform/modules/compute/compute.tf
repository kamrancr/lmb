module "asg" {
    source = "./asg"
    web_ami = "${var.web_ami}"
    web_instance_type = "${var.web_instance_type}"
    web_key_name = "${var.web_key_name}"
    private_subnets = "${var.private_subnets}"
    web_security_group_ids = "${var.web_security_group_ids}"
    web_min = "${var.web_min}"
    web_max = "${var.web_max}"
    web_elb_id = "${module.elb.web_elb_id}"

    app_ami = "${var.app_ami}"
    app_instance_type = "${var.app_instance_type}"
    app_key_name = "${var.app_key_name}"
    app_security_group_ids = "${var.app_security_group_ids}"
    app_min = "${var.app_min}"
    app_max = "${var.app_max}"
    app_elb_id = "${module.elb.app_elb_id}"

    name = "${var.name}"
    owner = "${var.owner}"
}

module "elb" {
    source = "./elb"
    public_subnet_ids = "${var.public_subnet_ids}"
    web_security_groups = "${var.web_security_groups}"

    private_subnet_ids = "${var.private_subnet_ids}"
    app_security_groups = "${var.app_security_groups}"

    dns_zone = "${var.dns_zone}"
    env = "${var.env}"

    create_bastion = "${var.create_bastion}"
    bastion_sg_ids = "${var.bastion_sg_ids}"
}

module "bastion" {
    source = "./bastion"
    bastion_instance_type = "${var.bastion_instance_type}"
    bastion_key_name = "${var.bastion_key_name}"
    bastion_sg_ids = "${var.bastion_sg_ids}"

    create_bastion = "${var.create_bastion}"
    bastion_min = "${var.bastion_min}"
    bastion_max = "${var.bastion_max}"
    bastion_elb_id = "${module.elb.bastion_elb_id}"
    public_subnets = "${var.public_subnets}"

    name = "${var.name}"
    owner = "${var.owner}"
}