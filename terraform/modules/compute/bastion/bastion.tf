data "aws_ami" "amzn2" {
    most_recent = true
    filter {
        name = "name"
        values = ["*amzn2-ami-hvm-2017.12.*"]
    }

    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }

    filter {
        name = "root-device-type"
        values = ["ebs"]
    }

    owners = ["137112412989"]
}

resource "aws_launch_configuration" "bastion" {
    name_prefix = "${var.name}-Bastion-"
    image_id = "${data.aws_ami.amzn2.id}"
    instance_type = "${var.bastion_instance_type}"
    key_name = "${var.bastion_key_name}"
    security_groups = ["${var.bastion_sg_ids}"]
    lifecycle {
        create_before_destroy = true
    }

    count = "${var.create_bastion}"
}

resource "aws_autoscaling_group" "bastion" {
    name = "${var.name}-Bastion-ASG"
    launch_configuration = "${aws_launch_configuration.bastion.name}"
    min_size = "${var.bastion_min}"
    max_size = "${var.bastion_max}"
    load_balancers = ["${var.bastion_elb_id}"]
    health_check_type = "ELB"
    health_check_grace_period = 300
    vpc_zone_identifier = ["${var.public_subnets}"]
    lifecycle {
        create_before_destroy = true
    }

    tags = [
        {
            key = "Name"
            value = "${var.name}-Bastion"
            propagate_at_launch = true
        },
        {
            key = "Owner"
            value = "${var.owner}"
            propagate_at_launch = true
        }
    ]

    count = "${var.create_bastion}"
}