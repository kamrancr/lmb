variable "bastion_instance_type" {}
variable "bastion_key_name" {}
variable "bastion_sg_ids" { type = "list" }

variable "bastion_min" {}
variable "bastion_max" {}
variable "bastion_elb_id" {}
variable "public_subnets" { type = "list" }

variable "name" {
    description = "Name of the object"
    type = "string"
}

variable "owner" {
    description = "Owner of the object"
    default = "Terraform"
    type = "string"
}

variable "create_bastion" {}