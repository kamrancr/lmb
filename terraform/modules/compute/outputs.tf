output "web_elb_id" {
    value = "${module.elb.web_elb_id}"
}

output "web_elb_dns_name" {
    value = "${module.elb.web_elb_dns_name}"
}

output "app_elb_id" {
    value = "${module.elb.app_elb_id}"
}

output "app_elb_dns_name" {
    value = "${module.elb.app_elb_dns_name}"
}

output "bastion_elb_id" {
    value = "${module.elb.bastion_elb_id}"
}