output "web_elb_id" {
    value = "${aws_elb.elb-web.id}"
}

output "web_elb_dns_name" {
    value = "${aws_elb.elb-web.dns_name}"
}

output "app_elb_id" {
    value = "${aws_elb.elb-app.id}"
}

output "app_elb_dns_name" {
    value = "${aws_elb.elb-app.dns_name}"
}

output "bastion_elb_id" {
    value = "${join("", aws_elb.bastion.*.id)}"
}