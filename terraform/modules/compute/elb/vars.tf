variable "public_subnet_ids" { type = "list" }
variable "web_security_groups" { type = "list" }

variable "private_subnet_ids" { type = "list" }
variable "app_security_groups" { type = "list" }

variable "dns_zone" {}
variable "env" {}

variable "bastion_sg_ids" { type = "list" }

variable "create_bastion" {}