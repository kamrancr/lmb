resource "aws_elb" "elb-web" {
    subnets = ["${var.public_subnet_ids}"]
    security_groups = ["${var.web_security_groups}"]

    listener {
        instance_port = 80
        instance_protocol = "http"
        lb_port = 80
        lb_protocol = "http"
    }
}

resource "aws_elb" "elb-app" {
    subnets = ["${var.private_subnet_ids}"]
    security_groups = ["${var.app_security_groups}"]
    internal = true

    listener {
        instance_port = 80
        instance_protocol = "http"
        lb_port = 80
        lb_protocol = "http"
    }
}

data "aws_route53_zone" "colab" {
    name = "${var.dns_zone}."
}

resource "aws_route53_record" "colab" {
    zone_id = "${data.aws_route53_zone.colab.zone_id}"
    name = "${var.env}"
    type = "CNAME"
    ttl = "5"
    records = ["${aws_elb.elb-web.dns_name}"]
}

resource "aws_elb" "bastion" {
    subnets = ["${var.public_subnet_ids}"]
    security_groups = ["${var.bastion_sg_ids}"]

    listener {
        instance_port = 22
        instance_protocol = "tcp"
        lb_port = 1234
        lb_protocol = "tcp"
    }

    count = "${var.create_bastion}"
}

resource "aws_route53_record" "bastion" {
    zone_id = "${data.aws_route53_zone.colab.zone_id}"
    name = "bastion.${var.env}"
    type = "CNAME"
    ttl = "5"
    records = ["${aws_elb.bastion.dns_name}"]

    count = "${var.create_bastion}"
}