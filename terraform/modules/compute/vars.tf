/* ASG */
variable "web_ami" {}
variable "web_instance_type" {}
variable "web_key_name" {}
variable "web_security_group_ids" { type = "list" }
variable "web_min" {}
variable "web_max" {}

variable "app_ami" {}
variable "app_instance_type" {}
variable "app_key_name" {}
variable "app_security_group_ids" { type = "list" }
variable "app_min" {}
variable "app_max" {}

variable "private_subnets" { type = "list" }

/* ELB */
variable "public_subnet_ids" { type = "list" }
variable "web_security_groups" {type = "list" }
variable "private_subnet_ids" { type = "list" }
variable "app_security_groups" { type = "list" }

variable "dns_zone" {}
variable "env" {}

/* Bastion */
variable "bastion_instance_type" {}
variable "bastion_key_name" {}
variable "bastion_sg_ids" { type = "list" }

variable "bastion_min" {}
variable "bastion_max" {}
variable "public_subnets" { type = "list" }

/* Generic */

variable "name" {
    description = "Name of the object"
    type = "string"
}

variable "owner" {
    description = "Owner of the object"
    default = "Terraform"
    type = "string"
}

variable "create_bastion" {}