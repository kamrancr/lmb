variable "web_ami" {}
variable "web_instance_type" {}
variable "web_key_name" {}
variable "web_security_group_ids" { type = "list" }
variable "web_min" {}
variable "web_max" {}
variable "web_elb_id" {}

variable "app_ami" {}
variable "app_instance_type" {}
variable "app_key_name" {}
variable "app_security_group_ids" { type = "list" }
variable "app_min" {}
variable "app_max" {}
variable "app_elb_id" {}

variable "private_subnets" { type = "list" }

variable "name" {
    description = "Name of the object"
    type = "string"
}

variable "owner" {
    description = "Owner of the object"
    default = "Terraform"
    type = "string"
}