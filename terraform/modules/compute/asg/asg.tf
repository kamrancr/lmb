resource "aws_launch_configuration" "web" {
    name_prefix = "${var.name}-Web-"
    image_id = "${var.web_ami}"
    instance_type = "${var.web_instance_type}"
    key_name = "${var.web_key_name}"
    security_groups = ["${var.web_security_group_ids}"]
    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_autoscaling_group" "web" {
    name = "${var.name}-Web-ASG"
    launch_configuration = "${aws_launch_configuration.web.name}"
    min_size = "${var.web_min}"
    max_size = "${var.web_max}"
    load_balancers = ["${var.web_elb_id}"]
    health_check_type = "ELB"
    health_check_grace_period = 300
    vpc_zone_identifier = ["${var.private_subnets}"]
    lifecycle {
        create_before_destroy = true
    }

    tags = [
        {
            key = "Name"
            value = "${var.name}-Web"
            propagate_at_launch = true
        },
        {
            key = "Owner"
            value = "${var.owner}"
            propagate_at_launch = true
        }
    ]
}

resource "aws_autoscaling_policy" "web-scale-up" {
    name = "${var.name}-web-scale-up"
    scaling_adjustment = 1
    adjustment_type = "ChangeInCapacity"
    cooldown = 300
    autoscaling_group_name = "${aws_autoscaling_group.web.name}"
}

resource "aws_autoscaling_policy" "web-scale-down" {
    name = "${var.name}-web-scale-down"
    scaling_adjustment = -1
    adjustment_type = "ChangeInCapacity"
    cooldown = 300
    autoscaling_group_name = "${aws_autoscaling_group.web.name}"
}

resource "aws_cloudwatch_metric_alarm" "web-requests-up" {
    alarm_name = "${var.name}-web-req-up"
    comparison_operator = "GreaterThanOrEqualToThreshold"
    evaluation_periods = "2"
    metric_name = "RequestCount"
    namespace = "AWS/ELB"
    period = "60"
    statistic = "Sum"
    threshold = "3500"
    alarm_actions = [
        "${aws_autoscaling_policy.web-scale-up.arn}"
    ]

    ok_actions = [
         "${aws_autoscaling_policy.web-scale-down.arn}"
    ]
    dimensions {
        LoadBalancerName = "${var.web_elb_id}"
    }
}

resource "aws_launch_configuration" "app" {
    name_prefix = "${var.name}-App-"
    image_id = "${var.app_ami}"
    instance_type = "${var.app_instance_type}"
    key_name = "${var.app_key_name}"
    security_groups = ["${var.app_security_group_ids}"]
    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_autoscaling_group" "app" {
    name = "${var.name}-App-ASG"
    launch_configuration = "${aws_launch_configuration.app.name}"
    min_size = "${var.app_min}"
    max_size = "${var.app_max}"
    load_balancers = ["${var.app_elb_id}"]
    health_check_type = "ELB"
    health_check_grace_period = 300
    vpc_zone_identifier = ["${var.private_subnets}"]
    lifecycle {
        create_before_destroy = true
    }

    tags = [
        {
            key = "Name"
            value = "${var.name}-App"
            propagate_at_launch = true
        },
        {
            key = "Owner"
            value = "${var.owner}"
            propagate_at_launch = true
        }
    ]
}

resource "aws_autoscaling_policy" "app-scale-up" {
    name = "${var.name}-app-scale-up"
    scaling_adjustment = 1
    adjustment_type = "ChangeInCapacity"
    cooldown = 300
    autoscaling_group_name = "${aws_autoscaling_group.app.name}"
}

resource "aws_autoscaling_policy" "app-scale-down" {
    name = "${var.name}-app-scale-down"
    scaling_adjustment = -1
    adjustment_type = "ChangeInCapacity"
    cooldown = 300
    autoscaling_group_name = "${aws_autoscaling_group.app.name}"
}

resource "aws_cloudwatch_metric_alarm" "app-requests-up" {
    alarm_name = "${var.name}-app-req-up"
    comparison_operator = "GreaterThanOrEqualToThreshold"
    evaluation_periods = "2"
    metric_name = "RequestCount"
    namespace = "AWS/ELB"
    period = "60"
    statistic = "Sum"
    threshold = "3500"
    alarm_actions = [
        "${aws_autoscaling_policy.app-scale-up.arn}"
    ]

    ok_actions = [
        "${aws_autoscaling_policy.app-scale-down.arn}"
    ]
    dimensions {
        LoadBalancerName = "${var.app_elb_id}"
    }
}