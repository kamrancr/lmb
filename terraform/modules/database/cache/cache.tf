resource "aws_elasticache_subnet_group" "cache" {
    name = "${var.name}-Cache-SubGroup"
    subnet_ids = ["${var.subnet_ids}"]
}

resource "aws_elasticache_cluster" "cache" {
    cluster_id = "${lower(var.name)}-cache"
    engine = "${var.engine}"
    node_type = "${var.node_type}"
    port = "${var.port}"
    num_cache_nodes = 1
    parameter_group_name = "${var.pg_name}"
    subnet_group_name = "${aws_elasticache_subnet_group.cache.name}"
    security_group_ids = ["${var.sg_id}"]
}