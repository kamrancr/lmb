variable "subnet_ids" { type = "list" }

variable "engine" {}
variable "node_type" {}
variable "port" {}
variable "pg_name" {}
variable "sg_id" {}

variable "name" {}