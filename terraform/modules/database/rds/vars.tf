variable "subnet_ids" { type = "list" }

variable "db_instance" {}
variable "db_name" {}
variable "db_user" {}
variable "db_password" {}

variable "skip_snapshot" {}

variable "sg_id" {}

variable "name" {}
variable "owner" {}