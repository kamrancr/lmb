resource "aws_db_subnet_group" "rds" {
    subnet_ids = ["${var.subnet_ids}"]
    tags {
        Name = "${var.name}"
        Owner = "${var.owner}"
    }
}

resource "aws_db_instance" "mysql" {
    allocated_storage = 10
    storage_type = "gp2"
    engine = "mysql"
    engine_version = "5.7.19"
    instance_class = "${var.db_instance}"
    name = "${var.db_name}"
    username = "${var.db_user}"
    password = "${var.db_password}"
    db_subnet_group_name = "${aws_db_subnet_group.rds.id}"
    vpc_security_group_ids = ["${var.sg_id}"]
    skip_final_snapshot = "${var.skip_snapshot}"
}