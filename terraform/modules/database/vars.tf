variable "subnet_ids" { type = "list" }

variable "db_instance" {}
variable "db_name" {}
variable "db_user" {}
variable "db_password" {}
variable "skip_snapshot" {}

variable "sg_id" {}

variable "name" {}
variable "owner" {}

variable "engine" {}
variable "node_type" {}
variable "port" {}
variable "pg_name" { default = "default.memcached1.4" }