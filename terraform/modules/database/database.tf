module "rds" {
    source = "./rds"
    subnet_ids = ["${var.subnet_ids}"]
    db_instance = "${var.db_instance}"
    db_name = "${var.db_name}"
    db_user = "${var.db_user}"
    db_password = "${var.db_password}"
    skip_snapshot = "${var.skip_snapshot}"

    sg_id = "${var.sg_id}"

    name = "${var.name}"
    owner = "${var.owner}"
}

module "cache" {
    source = "./cache"
    subnet_ids = ["${var.subnet_ids}"]
    engine = "${var.engine}"
    node_type = "${var.node_type}"
    port = "${var.port}"
    pg_name = "${var.pg_name}"
    sg_id = "${var.sg_id}"
    name = "${var.name}"
}