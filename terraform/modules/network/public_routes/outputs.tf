output "sg_id" {
    value = "${aws_security_group.public.id}"
}

output "bastion_sg_id" {
    value = "${join(" ", aws_security_group.bastion.*.id)}"
}