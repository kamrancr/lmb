resource "aws_internet_gateway" "igw" {
    vpc_id = "${var.vpc_id}"

    tags {
        Name = "${var.name}-IGW"
        Owner = "${var.owner}"
    }
}

resource "aws_route" "internet_access_pub" {
    route_table_id = "${var.rtb_id}"
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
}

resource "aws_route_table_association" "rtb_assoc" {
    subnet_id = "${element(var.subnet_ids, count.index)}"
    route_table_id = "${var.rtb_id}"

    count = "${var.count}"
}

resource "aws_security_group" "public" {
    vpc_id = "${var.vpc_id}"

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags {
        Name = "${var.name}-Pub"
    }
}

resource "aws_security_group" "bastion" {
    vpc_id = "${var.vpc_id}"

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 1234
        to_port = 1234
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags {
        Name = "${var.name}-Bastion"
    }

    count = "${var.create_bastion}"
}