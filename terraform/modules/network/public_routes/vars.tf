variable "vpc_id" {}
variable "rtb_id"  {}
variable "subnet_ids" { type = "list" }

variable "name" {
    description = "Name of the object"
    type = "string"
}

variable "owner" {
    description = "Owner of the object"
    default = "Terraform"
    type = "string"
}

variable "count" {}

variable "create_bastion" {}