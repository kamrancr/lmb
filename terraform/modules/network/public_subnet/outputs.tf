output "cidrs" {
    value = ["${aws_subnet.public.*.cidr_block}"]
}

output "subnet_ids" {
    value = ["${aws_subnet.public.*.id}"]
}