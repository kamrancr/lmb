resource "aws_subnet" "public" {
    vpc_id = "${var.vpc_id}"
    cidr_block = "${element(var.public_subnets, count.index)}"
    availability_zone = "${element(var.az, count.index)}"

    tags {
        Name = "${var.name}-Pub-${count.index + 1}"
        Owner = "${var.owner}"
    }

    count = "${var.count}"
}