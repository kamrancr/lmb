variable "vpc_id" {}
variable "private_subnets" { type = "list" }
variable "az" { type = "list" }

variable "name" {
    description = "Name of the object"
    type = "string"
}

variable "owner" {
    description = "Owner of the object"
    default = "Terraform"
    type = "string"
}

variable "count" {}