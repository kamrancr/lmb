output "cidrs" {
    value = ["${aws_subnet.private.*.cidr_block}"]
}

output "subnet_ids" {
    value = ["${aws_subnet.private.*.id}"]
}