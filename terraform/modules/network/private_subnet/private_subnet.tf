resource "aws_subnet" "private" {
    vpc_id = "${var.vpc_id}"
    cidr_block = "${element(var.private_subnets, count.index)}"
    availability_zone = "${element(var.az, count.index)}"

    tags {
        Name = "${var.name}-Priv-${count.index + 1}"
        Owner = "${var.owner}"
    }

    count = "${var.count}"
}