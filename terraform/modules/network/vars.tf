/* VPC */
variable "vpc_cidr" {}
variable "vpc_dns" {}

/* Public Subnet */
variable "public_subnets" { type = "list" }

/* Private Subnets */
variable "private_subnets" { type = "list" }

/* Generic */
variable "name" {}
variable "owner" {}
variable "count" {}
variable "az" { type = "list" }

variable "create_bastion" {}