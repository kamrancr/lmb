resource "aws_vpc" "vpc" {
    cidr_block = "${var.cidr_block}"
    enable_dns_support = "${var.enable_dns}"
    enable_dns_hostnames = "${var.enable_dns}"

    tags {
        Name = "${var.name}-VPC"
        Owner = "${var.owner}"
    }
}