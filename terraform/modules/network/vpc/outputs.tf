output "vpc_id" {
    value = "${aws_vpc.vpc.id}"
}

output "vpc_rtb_id" {
    value = "${aws_vpc.vpc.main_route_table_id}"
}