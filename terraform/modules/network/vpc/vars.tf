variable "cidr_block" {}
variable "enable_dns" {}

variable "name" {
    description = "Name of the object"
    type = "string"
}

variable "owner" {
    description = "Owner of the object"
    default = "Terraform"
    type = "string"
}