output "nat_eips" {
    value = ["${aws_nat_gateway.public.*.public_ip}"]
}

output "sg_id" {
    value = "${aws_security_group.private.id}"
}