resource "aws_nat_gateway" "public" {
    subnet_id = "${element(var.public_subnet_ids, count.index)}"
    allocation_id = "${element(aws_eip.nat.*.id, count.index)}"

    tags {
        Name = "${var.name}-NGW-${count.index + 1}"
        Owner = "${var.owner}"
    }

    count = "${var.count}"
}

resource "aws_eip" "nat" {
    vpc = "true"

    tags {
        Name = "${var.name}-EIP-${count.index + 1}"
        Owner = "${var.owner}"
    }

    count = "${var.count}"
}

resource "aws_route_table" "private" {
    vpc_id = "${var.vpc_id}"
    tags {
        Name = "${var.name}-Priv-${count.index + 1}"
        Owner = "${var.owner}"
    }

    count = "${var.count}"
}

resource "aws_route" "private" {
    route_table_id = "${element(aws_route_table.private.*.id, count.index)}"
    destination_cidr_block = "0.0.0.0/0"
    nat_gateway_id = "${element(aws_nat_gateway.public.*.id, count.index)}"

    count = "${var.count}"
}

resource "aws_route_table_association" "private" {
    subnet_id = "${element(var.subnet_ids, count.index)}"
    route_table_id = "${element(aws_route_table.private.*.id, count.index)}"

    count = "${var.count}"
}

resource "aws_network_acl" "private" {
    vpc_id = "${var.vpc_id}"
    subnet_ids = ["${var.subnet_ids}"]

    egress {
        rule_no = 100
        action = "allow"
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_block = "0.0.0.0/0"
    }

    ingress {
        protocol = "-1"
        rule_no = 100
        from_port = 0
        to_port = 0
        action = "allow"
        cidr_block = "0.0.0.0/0"
    }
}

resource "aws_security_group" "private" {
    name = "${var.name}-Priv"
    vpc_id = "${var.vpc_id}"

    ingress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags {
        Name = "${var.name}-Priv"
    }
}