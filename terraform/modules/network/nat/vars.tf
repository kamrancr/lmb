variable "count" {
    description = "The number of NAT GWs and EIPs to create"
}

variable "name" {
    description = "Name of the object"
    type = "string"
}

variable "owner" {
    description = "Owner of the object"
    default = "Terraform"
    type = "string"
}

variable "vpc_id" {}
variable "subnet_ids" { type = "list" }
variable "public_subnet_ids" { type = "list" }
