module "vpc" {
    source = "./vpc"
    cidr_block = "${var.vpc_cidr}"
    enable_dns = "${var.vpc_dns}"

    name = "${var.name}"
    owner = "${var.owner}"
}

module "public_subnet" {
    source = "./public_subnet"
    vpc_id = "${module.vpc.vpc_id}"
    public_subnets = "${var.public_subnets}"
    az = "${var.az}"
    
    name = "${var.name}"
    owner = "${var.owner}"
    
    count = "${var.count}"
}

module "public_routes" {
    source = "./public_routes"
    vpc_id = "${module.vpc.vpc_id}"
    rtb_id = "${module.vpc.vpc_rtb_id}"
    subnet_ids = "${module.public_subnet.subnet_ids}"

    name = "${var.name}"
    owner = "${var.owner}"

    count = "${var.count}"

    create_bastion = "${var.create_bastion}"
}

module "private_subnet" {
    source = "./private_subnet"
    vpc_id = "${module.vpc.vpc_id}"
    private_subnets = "${var.private_subnets}"
    az = "${var.az}"

    name = "${var.name}"
    owner = "${var.owner}"
    
    count = "${var.count}"
}

module "nat" {
    source = "./nat"
    vpc_id = "${module.vpc.vpc_id}"
    subnet_ids = "${module.private_subnet.subnet_ids}"
    public_subnet_ids = "${module.public_subnet.subnet_ids}"

    name = "${var.name}"
    owner = "${var.owner}"

    count = "${var.count}"
}