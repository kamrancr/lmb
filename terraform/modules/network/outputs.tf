output "vpc_id" {
    value = "${module.vpc.vpc_id}"
}

output "public_cidrs" {
    value = ["${module.public_subnet.cidrs}"]
}

output "public_subnet_ids" {
    value = ["${module.public_subnet.subnet_ids}"]
}

output "private_cidrs" {
    value = ["${module.private_subnet.cidrs}"]
}

output "private_subnet_ids" {
    value = ["${module.private_subnet.subnet_ids}"]
}

output "nat_eips" {
    value = ["${module.nat.nat_eips}"]
}

output "public_sg_id" {
    value = "${module.public_routes.sg_id}"
}

output "private_sg_id" {
    value = "${module.nat.sg_id}"
}

output "bastion_sg_id" {
    value = "${module.public_routes.bastion_sg_id}"
}